"""
Baseline solvers
"""

__author__ = 'mmihaltz'


def startingAtTheStop(stopstate, delta):
    """
    Just returns stopstate. This gives around 11% accuracy
    (measured on 150K set) and should give more
     if all-dead stop state instances are allowed in the gold standard.
    :param stopstate: rows of binary matrix, the stop state
    :param delta: integer, number of steps to go
    :return: the predicted starting state, format same as stopstate
    """
    return stopstate
