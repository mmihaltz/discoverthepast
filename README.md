## README

Discover The Past challenge

Marton Mihaltz <mmihaltz@gmail.com>

## Installation

*Requirements:*

- Python 2.7+
- Linux (if you want to test the simulation, see below)

To install, just clone this repository to your machine.

## Usage

### Solving for a set of test setups

To generate predicted start states for list of stop states & delta values given 
in a text file (input format of the challenge):

```
python solve.py <testfile>
```

This will write the predictions to stdout in the required format. 
If you want to write the output to a  file, use the `-o <outputfile>` switch.
 
`solve.py` is currently configured to use a baseline solving algorithm, 
`baseline.startingAtTheStop()` (see module documentation for more information).

### Generating a test set and evaluating

You can use the following command to generate 500 test instances conforming the 
task description:

```
python generate.py 500 myset
```

This will create 2 files:

* `myset.test`: 500 stop states with ids and the delta values used. 
Format is the same as input format for the task.
* `myset.train`: ids, delta values, start states and stop states for the 
same set. Each row is a space-separated list of 802 values. The start and stop 
states are represented using column-wise serialization.

The above command uses default settings: the delta values are between 1 and 5
and are randomly selected for each instance. The probability of random live 
cells at the creation of each setup is a random number between .01 and .99.
Both of these parameters can be customized with the command line parameters.
Run `python generate.py -h` for more information.

You can now run the solver on this dataset and evaluate its accuracy using 
the following command:

```
python solve.py myset.test -g myset.train -o myset.solve
```

This will generate solutions for each test instance and compare the predicted 
starting states to the actual starting states in `myset.train` (the gold standard). 
The accuracy, number of correctly predicted and total number of test instances 
are written to stdout:

```
Total instances: 500
Correctly predicted: 46
Accuracy: 0.092
```

Besides evaluation, the `.train` files are meant to be generated to obtain 
training instances for a machine learning-based solution.

### Testing the simulator

You can envoke a console-based Conway's Game of Life simulator that generates 
 a random initial setup (probability of initially live cells is chosen randomly 
 from [.01, .99]):
 
```
python game.py
```

After the state of a generation is displayed, you can press Enter to see 
the state of the next generation or ctrl-c to exit.

The API in `game.py` also supports testing the evolution of arbitrary user-supplied cell patterns, 
see the functions `test_blinker()` and `test_glider()`.