#!/usr/bin/env python

"""
Apply a solver to input file, dump results to stdout or file
"""

__author__ = 'mmihaltz'

import argparse
import re
import sys

import baseline
import game


def cols2rows(m):
    x = []
    for i in range(0, game.DEFAULT_BOARD_SIZE):
        row = []
        for col in m:
            row.append(col[i])
        x.append(row)
    return x


def iter_input(inputfile):
    """
    Generator that yields (id, delta, rows) tuples from MW format file
    :param inputfile: name of input file
    """
    state = 'O' # I|O
    lcnt = 0
    id = None
    delta = None
    columns = [] # matrix
    with open(inputfile) as inp:
        for line in inp:
            line = line.rstrip()
            lcnt += 1
            if state == 'O':
                m = re.match('^Id: (.+$)', line)
                if not m:
                    sys.exit('Error in input line {0}: expected "Id: <id>"'
                             .format(lcnt))
                id = m.group(1)
                state = 'I'
                continue
            elif state == 'I':
                if line == '':
                    if len(columns) != game.DEFAULT_BOARD_SIZE:
                        sys.exit('Error in input line {0}: too few columns'
                                 .format(lcnt))
                    # prepare for new instance
                    state = 'O'
                    id = None
                    delta = None
                    columns = []
                    continue
                elif line.startswith('delta: '):
                    if len(columns) != game.DEFAULT_BOARD_SIZE:
                        sys.exit('Error in input line {0}: delta unexptected'
                                 .format(lcnt))
                    vals = line.split(' ')
                    if len(vals) != 2:
                        sys.exit('Error in input line {0}: incorrect format'
                                 .format(lcnt))
                    delta = int(vals[1]) # exception if not int
                    # Ahhh finally:
                    yield id, delta, cols2rows(columns)
                    continue
                else:
                    if not re.match('^Column\d(\d?): .+', line):
                        sys.exit('Error in input line {0}: expected "ColumnXX: ..."'
                                 .format(lcnt))
                    vals = line.split(' ')
                    if len(vals) != game.DEFAULT_BOARD_SIZE + 1:
                        sys.exit('Error in input line {0}: incorrect column size {1}'
                                 .format(lcnt, len(vals)))
                    col = []
                    for v in vals[1:]:
                        if v not in ['0', '1']:
                            sys.exit('Error in input line {0}: incorrect value {1}'
                                 .format(lcnt, v))
                        col.append(int(v))
                    columns.append(col)
                    continue


def iter_gold(goldfile):
    """
    Generator iterator for gold standard file that yields
    (id, delta, rows_of_gold_start_state, rows_of_gold_stop_state)
    :param file: gold standard file name
    file format: see generate.generate()
    """
    lcnt = 0
    with open(goldfile) as gold:
        for line in gold:
            lcnt += 1
            line = line.rstrip()
            vals = line.split(' ')
            if len(vals) != 2 + 2 * game.DEFAULT_BOARD_SIZE * game.DEFAULT_BOARD_SIZE:
                sys.exit('Error in gold line {0}: incorrect size {1}'
                         .format(lcnt, len(vals)))
            id = vals[0]
            delta = int(vals[1])
            # gold start state rows
            startdata = vals[2:game.DEFAULT_BOARD_SIZE * game.DEFAULT_BOARD_SIZE + 2]
            startrows = []
            for i in range(0, game.DEFAULT_BOARD_SIZE):
                row = []
                for j in range(0, game.DEFAULT_BOARD_SIZE):
                    row.append(int(startdata[j * game.DEFAULT_BOARD_SIZE + i]))
                startrows.append(row)
            # gold stop state rows
            stopdata = vals[2 + game.DEFAULT_BOARD_SIZE * game.DEFAULT_BOARD_SIZE:]
            stoprows = []
            for i in range(0, game.DEFAULT_BOARD_SIZE):
                row = []
                for j in range(0, game.DEFAULT_BOARD_SIZE):
                    row.append(int(stopdata[j * game.DEFAULT_BOARD_SIZE + i]))
                stoprows.append(row)
            yield (id, delta, startrows, stoprows)


def same(rows1, rows2):
    """
    Return True iff all values in the 2 matrices are equal.
    No size checking.
    """
    for x in range(0, game.DEFAULT_BOARD_SIZE):
        for y in range(0, game.DEFAULT_BOARD_SIZE):
            if rows1[x][y] != rows2[x][y]:
                return False
    return True


def dump_state(msg, cells, out=sys.stdout, alive='X', dead='.'):
    print(msg)
    for row in cells:
        out.write('{0}\n'.format(' '.join(
            [(alive if c == 1 else dead) for c in row])))


def apply_solver(inputfile, outputfile, solver, goldfile):
    """
    Read test instances from input, apply solver, write predictions
    to output in MW format.
    :param inputfile: name of input file
    :param outputfile: name of output file or None (stdout will be used)
    :param solver: a function: (stopstate, delta) => startstate
    :param goldfile: if not None, also perform evaluation against this
    :return: None
    goldfile must have same number of instances with the same ids as inputfile
    in the same order. goldfile format: see generate.generate()
    """
    out = sys.stdout if outputfile is None else file(outputfile, 'w')
    gold = None if goldfile is None else iter_gold(goldfile)
    total = 0
    correct = 0
    for id, delta, stop in iter_input(inputfile):
        # get prediction
        start = solver(stop, delta)
        #dump_state('id={} stop:'.format(id), stop)
        #dump_state('id={}, delta={} predicted start:'.format(id, delta), start)
        # save prediction
        b = game.Board()
        b.cells = start
        b.export(out, id)
        out.write('\n')
        # evaluate if needed
        if goldfile is not None:
            try:
                # check id vs. goldid
                goldid, golddelta, goldstart, goldstop = gold.next()
                #dump_state('id={} gold start:'.format(goldid), goldstart)
                #dump_state('id={} gold stop:'.format(goldid), goldstop)
                if id != goldid:
                    sys.exit('Error: input id {0} vs. gold id {1} mismatch'
                             .format(id, goldid))
                total += 1
                if same(start, goldstart):
                    correct += 1
            except StopIteration:
                sys.exit('Error: input vs. gold standard size mismatch')
    if goldfile is not None:
        accuracy = 0.0 if total == 0 else float(correct) / float(total)
        sys.stderr.write('Total instances: {0}\n'.format(total))
        sys.stderr.write('Correctly predicted: {0}\n'.format(correct))
        sys.stderr.write('Accuracy: {0}\n'.format(accuracy))


def main():
    parser = argparse.ArgumentParser(
        description='Predict start states for stop states and delta')
    parser.add_argument('inputfile',
                        help='input file name')
    parser.add_argument('-o', '--outputfile', default=None,
                        help='output file name; if omitted stdout is used')
    parser.add_argument('-g', '--goldfile', default=None,
                        help='name of file with outputs to evaluate against')
    args = parser.parse_args()
    apply_solver(args.inputfile,
                 args.outputfile,
                 baseline.startingAtTheStop,
                 args.goldfile)


if __name__ == '__main__':
    main()
