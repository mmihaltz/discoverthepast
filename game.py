#!/usr/bin/env python

"""
Conway's Game of Life.
"""

import copy
import random
import os
import sys

__author__ = 'mmihaltz'

DEFAULT_BOARD_SIZE = 20


class Board:
    """
    A finite size GoL board.
    Edge and corner cells' off-board neighbors are assumed to be dead.
    """

    def __init__(self,
                 x_size=DEFAULT_BOARD_SIZE,
                 y_size=DEFAULT_BOARD_SIZE):
        """Initialize board with given size.
        :param x_size: number of rows(!)
        :param y_size: number of columns(!)
        self.cell[x][y] refers to cell in x'th row, y'th column(!)
        """
        self.x_size = x_size
        self.y_size = y_size
        self.age = 0
        self.cells = [[0] * y_size for i in range(0, x_size)]

    def dump(self,
             out=sys.stdout,
             alive='X',
             dead='.',
             debug=False):
        """
        Write text representation of board to file-like object out.
        """
        out.write('Age: {0}\n'.format(self.age))
        if not debug:
            for row in self.cells:
                out.write('{0}\n'.format(' '.join(
                    [(alive if c == 1 else dead) for c in row])))
        else:
            for x in range(0, self.x_size):
                for y in range(0, self.y_size):
                    out.write((alive if self.cells[x][y] == 1 else dead) + ' ')
                out.write('    ')
                for y in range(0, self.y_size):
                    out.write(str(self.sum_neighborhood(x, y)) + ' ')
                out.write('\n')

    def export(self, file, id):
        """
        Write cell state to output file in MW format
        :param file: open file object
        :param id: id to write to "Id:" field
        """
        file.write('Id: {0}\n'.format(id))
        for y in range(0, self.y_size):
            file.write('Column{0}: {1}\n'.format(y+1,
                                               ' '.join(
                                                   [str(self.cells[x][y])
                                                    for x in
                                                    range(0, self.x_size)]))
                       )

    def evolve(self, delta=1):
        """
        Advance simulation by delta generations.
        """
        for d in range(0, delta):
            tmp = copy.deepcopy(self.cells)
            for x in range(0, self.x_size):
                for y in range(0, self.y_size):
                    s = self.sum_neighborhood(x, y)
                    if s == 3:
                        tmp[x][y] = 1
                    elif s != 4:
                        tmp[x][y] = 0
            self.cells = copy.deepcopy(tmp)
            self.age += 1

    def sum_neighborhood(self, x, y):
        """
        Returns the sum of the values of all neighbors of cell at (x,y)
        plus the value of (x,y) (!)
        """
        if x < 0 or x >= self.x_size or y < 0 or y >= self.y_size:
            raise Exception('Out of range parameters {0}, {1}'
                            .format(x, y))
        c = 0
        for xn in [x-1, x, x+1]:
            for yn in [y-1, y, y+1]:
                # corner and edge cell's off-board neighbors
                if xn < 0 or xn >= self.x_size or yn < 0 or yn >= self.y_size:
                    continue
                c += self.cells[xn][yn]
        return c

    def isAllDead(self):
        """
        :return: True iff all cells on board are dead
        """
        for row in self.cells:
            for y in range(0, len(row)):
                if row[y] == 1:
                    return False
        return True

    def play(self, debug=False):
        """Start infinite loop: dump to stdout, wait for Enter, evolve, repeat
        """
        while True:
            os.system('clear')
            self.dump(debug=debug)
            raw_input('\n(Press Enter for next generation, ctrl+c to quit)')
            self.evolve()

    def addPattern(self, x, y, pat):
        """Insert matrix pat at x'th row y'th column"""
        for i, row in enumerate(pat):
            for j, c in enumerate(row):
                self.cells[x+i][y+j] = c


def createRandomBoard(palive,
                      x_size=DEFAULT_BOARD_SIZE,
                      y_size=DEFAULT_BOARD_SIZE):
    """
    Return a new Board with palive probability of random live cells.
    palive=1.0 means all cells are alive, palive=0 means all cells are
    dead. Other parameters: see Board constructor.
    :param palive: must be in [0, 1.0]
    """
    if palive < 0.0 or palive > 1.0:
        raise Exception('Error: invalid palive value {0}'.format(palive))
    board = Board(x_size, y_size)
    for w in random.sample(range(0, x_size * y_size),
                           int(palive * x_size * y_size)):
        x = int(w / x_size)
        y = w % x_size
        board.cells[x][y] = 1
    return board


def infiniteRandomGame():
    """
    Works on Linux OS'es only (because of clear screen method)
    :param palive: see Board.createRandomBoard()
    """
    board = createRandomBoard(random.uniform(.01, .99))
    board.play()


def test_blinker():
    board = Board(4, 4)
    board.addPattern(0, 0,
                    [[1,1,0,0],
                     [1,1,0,0],
                     [0,0,1,1],
                     [0,0,1,1]])
    board.play(debug=True)


def test_glider():
    board = Board(20, 20)
    board.addPattern(0, 0,
                    [[0,1,0],
                     [0,0,1],
                     [1,1,1]])
    board.play()


def main():
    """
    Testing on stdout
    """
    #test_blinker()
    #test_glider()
    infiniteRandomGame()


if __name__ == '__main__':
    main()
