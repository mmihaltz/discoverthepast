#!/usr/bin/env python
"""
Generate random generated GoL boards and their evolved states for training
and testing and write to files
"""

import argparse
import copy
import logging
import random
import sys

import game

__author__ = 'mmihaltz'

# default function to create a random initial alive probability
fpalive_rand = lambda: random.uniform(.01, .99)

# default function to create a random delta
fdelta_rand = lambda min, max: random.randrange(min, max)


def generate(filename, n, dmin, dmax, prob):
    """
    Generate 2 files:
    filename.test: only the end states
    filename.train: end states and predecessor states
    Test file format: see MW task specification
    Train file format: id, delta, start & stop in column-wise format (
    802 fields)
    Parameters: see main()
    """
    ftrain = open(filename + '.train', 'w')
    ftest = open(filename + '.test', 'w')
    i = 0
    while i < n:
        delta = fdelta_rand(dmin, dmax)
        initprob = prob if prob is not None else fpalive_rand()
        logging.debug('delta={0}, prob={1}\n'.format(delta, initprob))
        board = game.createRandomBoard(initprob)
        board.evolve(5)
        start = copy.deepcopy(board.cells)
        board.evolve(delta)
        # run again if all dead at stop
        if board.isAllDead():
            logging.debug('-Oops, all dead, retrying\n')
            continue
        stop = copy.deepcopy(board.cells)
        # Write id, delta, stop state to test file
        board.export(ftest, i)
        ftest.write('delta: {0}\n\n'.format(delta))
        # write id, delta, start + stop state to train file
        ftrain.write('{0} {1}'.format(i, delta))
        for y in range(0, board.y_size):
            ftrain.write(' ')
            ftrain.write(' '.join([str(start[x][y])
                                   for x in range(0, board.x_size)]))
        for y in range(0, board.y_size):
            ftrain.write(' ')
            ftrain.write(' '.join([str(stop[x][y])
                                   for x in range(0, board.x_size)]))
        ftrain.write('\n')
        i += 1


def main():
    #logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(description='Generate GoL boards')
    parser.add_argument('n', type=int,
                        help='number of items to generate')
    parser.add_argument('filename',
                        help='name of generated files')
    parser.add_argument('--dmin', type=int, default=1,
                        help='minimum delta (default: 1)')
    parser.add_argument('--dmax', type=int, default=5,
                        help='maximum delta (default: 5)')
    parser.add_argument('--prob', type=float, default=None,
                        help='initial alive probability (default: uniform random between .01 and .99)')
    args = parser.parse_args()
    generate(**vars(args))


if __name__ == '__main__':
    main()
